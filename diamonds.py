import sys
import itertools

ALPHABET = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

def get_char(index):
    return ALPHABET[index]

def get_index(char):
    return ALPHABET.index(char)

def cnt_space_inner(index):
    if index == 1:
        return 1
    elif index > 1:
        return (index - 1) * 2 + 1
    else:
        raise ValueError

def cnt_space_before(index, index_max):
    return index_max - index

def compose_line(index, index_max, space, payload):
    if index == 0:
        return f"{space * cnt_space_before(index, index_max)}{str(payload)}"
    elif index > 0:
        return f"{space * cnt_space_before(index, index_max)}{str(payload)}{space * cnt_space_inner(index)}{str(payload)}"
    else:
        raise ValueError

def draw_diamond(character_max, space):
    index_max = get_index(character_max)
    for i in itertools.chain(range(0, index_max), range(index_max, -1, -1)):
      print(compose_line(i, index_max, space, get_char(i)))

def main():
    draw_diamond(sys.argv[1], ".")

if __name__ == '__main__':
    main()